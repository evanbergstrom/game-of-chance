const possibleAnswers = ["Outlook not so good.", "You may rely on it.", "Signs point to yes.", "Better not tell you now.","It is known.", "It will not be.", "Your questions will be affirmed.", "Not the way I see it.", "It cannot be denied.", "It is ill-fated.", "Undoubtedly.", "The situation dictates no.", "It is certain.", "Concentrate and ask again."]

function eightBall () {
    var randAns = possibleAnswers[Math.floor(Math.random() * 14)];
    var answer = document.createElement("div");
    answer.textContent = randAns;
    var total = document.getElementById("answer");
    total.appendChild(answer);
}

